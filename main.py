from flask import Flask,render_template,request,jsonify
from flask_sqlalchemy import SQLAlchemy
import datetime
# creates a Flask application, named app
app = Flask(__name__)
import random, string

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/vault'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Entry(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(127), nullable=False)
    content = db.Column(db.String(10003),nullable=False)
    expiry_time =db.Column(db.DateTime,nullable=False)
    hash = db.Column(db.String(31),nullable=False)

with app.app_context():
    db.create_all()

@app.route("/")
def serve_index():
    return render_template('index.html')

@app.route("/create",methods=['POST'])
def create():
    hash = ''.join(random.choices(string.ascii_letters + string.digits, k=7))
    print(request.json["expiry_time"])
    expiry_time = datetime.datetime.now() + datetime.timedelta(hours=int(request.json["expiry_time"]))
    entry = Entry(title=request.json["title"],content = request.json["content"],expiry_time=expiry_time,hash=hash)
    db.session.add(entry)
    db.session.commit()
    return jsonify(entry.hash)

@app.route("/posts/<string:vault_hash>",methods=['GET'])
def get_post(vault_hash):
    entry = Entry.query.filter_by(hash=vault_hash).first()
    if entry==None:
        return render_template('404.html')
    if datetime.datetime.now() > entry.expiry_time:
        return render_template('expired.html')
    return render_template('post.html',entry_title=entry.title,entry_content=entry.content)


# run the application
if __name__ == "__main__":
    app.run(debug=True)