let editor;

async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
}

function validate(title, content, password) {
    let errors = [];
    if (title === '') {
        errors.push('Title cannot be empty');
    }
    if (title.length < 5) {
        errors.push('Title should have minimum 5 characters');
    }
    if (content === '') {
        errors.push('Content cannot be empty');
    }
    if (content.length < 5) {
        errors.push('Content should have minimum 5 characters');
    }
    if (password === '') {
        errors.push('Password cannot be empty');
    }
    if (password.length < 5) {
        errors.push('Password should have minimum 5 characters');
    }
    console.log(errors);
    return errors;
}

function onClick() {

    var SERVER_URI = document.location + "create";
    let password = document.getElementById("password").value;

    let errors = validate(document.getElementById("title").value, editor.getData(), password);

    if (errors.length == 0) {

        let title = CryptoJS.AES.encrypt(document.getElementById("title").value, password);
        let content = CryptoJS.AES.encrypt(editor.getData(), password);

        var data = {
            "title": title.toString(),
            "content": content.toString(),
            "expiry_time": document.getElementById("expiryTime").value,
        }

        postData(SERVER_URI, data)
            .then(data => {
                var url = document.location + "posts/" + data;
                document.getElementById("output").innerHTML = "Your Post was created at <a href='" + url + "'>" + url + "</a>";
                document.getElementById("title").value = "";
                document.getElementById("password").value = "";
                editor.setData('');
                document.getElementById("output").hidden = false;
                document.getElementById("error-list").hidden = true;
            });

    } else {
        // document.getElementById("error-list").innerHTML = errors.toString();
        document.getElementById("output").hidden = true;
        let ul = document.getElementById('errors-list');
        // document.getElementById('error-list').appendChild(ul);
        ul.innerHTML = "";
        errors.forEach(function (e) {
            let li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML += e;
        });
        document.getElementById("error-list").hidden = false;
    }



}

ClassicEditor
    .create(document.querySelector('#editor'))
    .then(newEditor => {
        editor = newEditor;
    })
    .catch(error => {
        console.error(error);
    });
